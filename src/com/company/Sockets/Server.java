package com.company.Sockets;

import java.io.DataInput;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by xw on 15.06.17.
 */
public class Server {

    public static void main(String[] args) throws IOException {

        ServerSocket serverSocket = new ServerSocket(7777);
        Socket socket = serverSocket.accept(); //here the app stop and wait data to socket
        InputStream istream = socket.getInputStream();
        DataInputStream dis = new DataInputStream(istream);
        System.out.println(dis.readUTF());
    }
}
