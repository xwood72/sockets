package com.company.Sockets;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;

/**
 * Created by xw on 15.06.17.
 */
public class Client {

    public static void main(String[] args) throws IOException{
        Socket socket = new Socket("127.0.0.1", 7777);
        OutputStream outputStream = socket.getOutputStream();
        DataOutputStream dataOutputStream = new DataOutputStream(outputStream);
        dataOutputStream.writeUTF("Ololo!");
        dataOutputStream.flush();
        socket.close();
    }



}
